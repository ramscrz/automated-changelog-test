package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

var i int = 80
var J int = 48

const (
	typeA = iota + 1
	typeB
	typeC
)

func main() {
	checkOSArgs()
	playWithVariables()
	playWithTypeConversion()
	playWithStrings()
	uniqo()
}

func checkOSArgs() map[int]string {
	fmt.Println(strings.Join(os.Args[1:], " "))
	fmt.Println(os.Args[1:])
	argMap := make(map[int]string)
	for index, arg := range os.Args[1:] {
		fmt.Printf("\nArgument #%v -:> %v", index, arg)
		argMap[index] = arg
	}
	fmt.Println(" ")

	return argMap
}

func uniq() {
	counter := make(map[string]int)
	var inputLines []string

	files := os.Args[1:]
	if len(files) == 0 {
		input := bufio.NewScanner(os.Stdin)
		for input.Scan() {
			inputLines = append(inputLines, input.Text())
		}
	} else {
		for index, file := range files {
			fmt.Printf("\nReading file %q [%d]...\n", file, index)
			openFile, err := os.Open(file)
			// openFile has type *os.File
			if err != nil {
				fmt.Printf("\n\tError: failed to open file.\n")
				fmt.Fprintf(os.Stderr, "\t\tuniq: %v\n", err)
			} else {
				input := bufio.NewScanner(openFile)
				for input.Scan() {
					inputLines = append(inputLines, input.Text())
				}
				openFile.Close()
			}
		}
	}

	for _, line := range inputLines {
		counter[line]++
	}

	fmt.Println(" ")
	for line, count := range counter {
		fmt.Printf("Line[%q] -> %d\n", line, count)
	}
	fmt.Println(" ")
}

func uniqo() {
	counter := make(map[string]int)
	var inputLines []string

	files := os.Args[1:]
	if len(files) != 0 {
		for index, file := range files {
			fmt.Printf("\nReading file %q [%d]...\n", file, index)
			fileData, err := ioutil.ReadFile(file)
			// openFile has type *os.File
			if err != nil {
				fmt.Printf("\n\tError: failed to read file.\n")
				fmt.Fprintf(os.Stderr, "\t\tuniqo: %v\n", err)
				continue
			}
			for _, line := range strings.Split(string(fileData), "\n") {
				inputLines = append(inputLines, line)
			}
		}
	}

	if len(inputLines) == 0 {
		input := bufio.NewScanner(os.Stdin)
		for input.Scan() {
			inputLines = append(inputLines, input.Text())
		}
	}

	for _, line := range inputLines {
		counter[line]++
	}

	fmt.Println(" ")
	for line, count := range counter {
		fmt.Printf("Line[%q] -> %d\n", line, count)
	}
	fmt.Println(" ")
}

func playWithVariables() {
	// Declares local variables
	var i int = 65
	var j float32
	j = 90.5
	k := 0

	var q, n, m int
	fmt.Printf("\nvar q -> (%v, %T)\n", q, q)
	fmt.Printf("\nvar n -> (%v, %T)\n", n, n)
	fmt.Printf("\nvar m -> (%v, %T)\n", m, m)

	var l, p, s = 66.6, true, "hello"
	fmt.Printf("\nvar l -> (%v, %T)\n", l, l)
	fmt.Printf("\nvar p -> (%v, %T)\n", p, p)
	fmt.Printf("\nvar s -> (%v, %T)\n", s, s)

	// Inspects variables (type and value)
	fmt.Println("Hello, Go playground!")
	fmt.Printf("PACKAGE main > var J -> %v, %T\n", J, J)

	fmt.Printf("typeA -> %v, %T\n", typeA, typeA)
	fmt.Printf("typeB -> %v, %T\n", typeB, typeB)
	fmt.Printf("typeC -> %v, %T\n", typeC, typeC)

	fmt.Printf("var i -> %v, %T\n", i, i)
	fmt.Printf("var j -> %v, %T\n", j, j)
	fmt.Printf("var k -> %v, %T\n", k, k)

	var (
		actorName    string  = "Mayumi"
		companion    string  = "Tobias"
		doctorNumber int     = 4
		season       int     = 1
		rating       float32 = 9.45
	)

	fmt.Println("\nActor Name: ", actorName, " -> CHECK")
	fmt.Println("\nCompanion: ", companion, " -> CHECK")
	fmt.Println("\nDoctor Number: ", doctorNumber, " -> CHECK")
	fmt.Println("\nSeason: ", season, " -> CHECK")
	fmt.Println("\nRating: ", rating, " -> CHECK")

	var stdCN complex64 = 1 + 2i
	fmt.Printf("\n%v, %T", stdCN, stdCN)
	fmt.Printf("\n%v, %T", real(stdCN), real(stdCN))
	fmt.Printf("\n%v, %T\n", imag(stdCN), imag(stdCN))

	var bigCN complex128 = 1 + 2i
	fmt.Printf("\n%v, %T", bigCN, bigCN)
	fmt.Printf("\n%v, %T", real(bigCN), real(bigCN))
	fmt.Printf("\n%v, %T\n", imag(bigCN), imag(bigCN))
}

func playWithTypeConversion() {
	// Converts variable types
	var t, j float32
	t = float32(i)
	j = 76.34
	fmt.Printf("var t -> %v, %T\n", t, t)

	var y int
	y = int(j)
	fmt.Printf("var y -> %v, %T\n", y, y)

	// Converts float to text/string.
	var text string
	text = fmt.Sprintf("%.6f", j)
	fmt.Printf("fmt.Sprintf -> var text -> %v, %T\n", text, text)

	// Converts integer to text/string.
	text = strconv.Itoa(y)
	fmt.Printf("Itoa -> var text -> %v, %T\n", text, text)

	// Converts text/string to integer.
	y = 33
	y, err := strconv.Atoi(text)
	if err != nil {
		fmt.Printf("Atoi -> err -> %v, %T\n", err, err)
	} else {
		fmt.Printf("Atoi -> success, var y -> %v, %T\n", y, y)
	}

	// Converts text/string to float32/float64.
	text = "33.45"
	u, err := strconv.ParseFloat(text, 32)
	if err != nil {
		fmt.Printf("ERROR > string to float: text = %v", text)
	} else {
		t = float32(u)
		fmt.Printf("SUCCESS > string to float: text = %v -> t = %v", text, t)
	}

}

func replaceStringChar(inText *string, index int, char uint8) {
	textRunes := []rune(*inText)
	textRunes[index] = rune(char)
	*inText = string(textRunes)
}

func playWithStrings() {
	var startText string = "This is the opening string."
	var endText string = "This is the closing string."
	fmt.Printf("\n%v\n", startText+" "+endText)
	var fullText string = startText + "*" + endText
	fmt.Printf("\nfullText -> %v, %T\n", fullText, fullText)
	var textBytes []byte = []byte(fullText)
	fmt.Printf("\ntextBytes -> %v, %T\n", textBytes, textBytes)
	textBytes[len(startText)] = ' '
	var textRunes []rune = []rune(fullText)
	fmt.Printf("\ntextRunes -> %v, %T\n", textRunes, textRunes)
	textRunes[len(startText)] = ' '

	replaceStringChar(&fullText, len(startText), ' ')
	fmt.Printf("\nfullText -> %v, %T\n", fullText, fullText)
}
