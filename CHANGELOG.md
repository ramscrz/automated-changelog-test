# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.2.0](https://github.com/ramscrz/automated-changelog-test/compare?from=$&to=) (2021-08-04)


### Features

* Add feature A to address F ([7f0ec04](https://gitlab.com/ramscrz/automated-changelog-test/commits/7f0ec04060ad0a6f8dc7691aa149752127df18a2))

### [3.1.1](https://github.com/ramscrz/automated-changelog-test/compare/v3.1.0...v3.1.1) (2021-08-04)


### Bug Fixes

* Fix error 500 in the authentication routine ([751432e](https://gitlab.com/ramscrz/automated-changelog-test/commits/751432e5473cca6211ce0c94efe14e8c297d43f3))

## [3.1.0](https://github.com/ramscrz/automated-changelog-test/compare/v3.0.0...v3.1.0) (2021-08-04)


### Features

* Implement feature Y to address requirement S ([4532ddc](https://gitlab.com/ramscrz/automated-changelog-test/commits/4532ddc78d11079828ab18e35ab5537fde7180c8))


### Bug Fixes

* Fix D applied to solve issue R ([6e32544](https://gitlab.com/ramscrz/automated-changelog-test/commits/6e32544e0ef3fb7ae73b96d0dd06e98f96f04335))

## [3.0.0](https://github.com/ramscrz/automated-changelog-test/compare/v2.2.0...v3.0.0) (2021-08-04)


### ⚠ BREAKING CHANGES

* 

### Features

* Implement feature F to allow users to load project files. ([3d6ca8b](https://gitlab.com/ramscrz/automated-changelog-test/commits/3d6ca8b3bbdeaecf8babeed6658b89c998d0d5e1))

## [2.2.0](https://github.com/ramscrz/automated-changelog-test/compare/v2.1.2...v2.2.0) (2021-08-04)


### Features

* Add feature X to allow users to access resource K ([64b875e](https://gitlab.com/ramscrz/automated-changelog-test/commits/64b875efe10565e4a8e25a5006602f5989ce7f16))

### [2.1.2](https://github.com/ramscrz/automated-changelog-test/compare/v2.1.1...v2.1.2) (2021-08-04)


### Bug Fixes

* Apply a new fix to address issue A. ([6277ff5](https://gitlab.com/ramscrz/automated-changelog-test/commits/6277ff587428b529d124eed1282799b45b402466))

### [2.1.1](https://github.com/ramscrz/automated-changelog-test/compare/v2.1.0...v2.1.1) (2021-08-01)


### Bug Fixes

* update .versionrc.json file. ([2708c67](https://gitlab.com/ramscrz/automated-changelog-test/commits/2708c677bc88d89913fb1931a33ab9f396a2667c))

## [2.1.0](https://gitlab.com/ramscrz/automated-changelog-test/compare/v2.0.2...v2.1.0) (2021-08-01)


### Features

* A minor feature is added. ([7eec748](https://gitlab.com/ramscrz/automated-changelog-test/commit/7eec7483fbdf009c5d615da4431f846badf7816c))

### [2.0.2](https://gitlab.com/ramscrz/automated-changelog-test/compare/v2.0.1...v2.0.2) (2021-08-01)

### Bug Fix

* Delete incorrect releases. ([a9df3c2](https://gitlab.com/ramscrz/automated-changelog-test/commit/a9df3c22443a7bb53689198bf5f3dbe0f99b8e1d))

## [2.0.0](https://gitlab.com/ramscrz/automated-changelog-test/compare/v1.0.0...v2.0.0) (2021-08-01)


### ⚠ BREAKING CHANGES

* store module has no more dependency to the inventory module.
* removes dependency to the Authentication module.

### Features

* implements the inventory in the store module. ([be4e9ba](https://gitlab.com/ramscrz/automated-changelog-test/commit/be4e9ba27782cdf8c7203e1db30b44a37afa0c50))
* implements the store functionality for selling items to players. ([5b4d50e](https://gitlab.com/ramscrz/automated-changelog-test/commit/5b4d50e8c44d9744132528df05d8edd8d0a08fe0))
* **lang:** add chinese language support. ([30b4863](https://gitlab.com/ramscrz/automated-changelog-test/commit/30b4863f68e1e2290ecf762b096555f39e57b516))
* **lang:** add japanese language. ([18a27b0](https://gitlab.com/ramscrz/automated-changelog-test/commit/18a27b0591e32db4798110b0dd406b4e8f8b297c))
* **setup:** standard-version added to project in order to automatically gnerate changelog. ([a24e487](https://gitlab.com/ramscrz/automated-changelog-test/commit/a24e487bb256ddb54440749d40c814bf99fbc6fc))


### Bug Fixes

* correct minor typos in code ([eaae335](https://gitlab.com/ramscrz/automated-changelog-test/commit/eaae3354547f8000bd53c4c7f03762824ed9ac4e))


* removes dependency to the Authentication module. ([40a8916](https://gitlab.com/ramscrz/automated-changelog-test/commit/40a89163efef52b187753be0ac2c40d51f2feca0))

## 1.0.0 (2021-07-31)
